package com.carrally.activity;

import android.app.Activity;
import android.os.Bundle;

import com.carrally.R;
import com.carrally.textstyle.BerlinLight;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginOptionActivity extends Activity {

    @Bind(R.id.header_title)
    BerlinLight title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_option);
        ButterKnife.bind(this);
        title.setText(getResources().getString(R.string.login));
    }

}
