package com.carrally.textstyle;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by iapp on 22/4/16.
 */
public class RobotoLight extends TextView {

    public RobotoLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RobotoLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RobotoLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "NewRocker-Regular.otf");
            setTypeface(tf);
        }
    }
}