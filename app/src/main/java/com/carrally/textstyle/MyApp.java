package com.carrally.textstyle;

import android.app.Application;
import android.support.multidex.MultiDex;

public class MyApp extends Application {

    public static final String TAG = MyApp.class
            .getSimpleName();
    private static MyApp mInstance;

    public MyApp() {

    }

    public static synchronized MyApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        MultiDex.install(this);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "NewRocker-Regular.otf"); // font from assets: "assets/interstarte-cond.ttf
    }

}