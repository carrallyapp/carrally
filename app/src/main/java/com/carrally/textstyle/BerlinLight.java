package com.carrally.textstyle;

/**
 * Created by bimal on 5/8/16.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class BerlinLight extends TextView {

    public BerlinLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BerlinLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BerlinLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Berlin-Sans-FB.ttf");
            setTypeface(tf);
        }
    }
}